customer(ida).
customer(jeremy).
customer(hugh).
customer(leroy).
customer(stella).

rose(cottage_beauty).
rose(golden_sunset).
rose(pink_paradise).
rose(sweet_dreams).
rose(mountain_bloom).

item(place_cards).
item(streamers).
item(balloons).
item(gourmet_chocolates).
item(candles).

event(senior_prom).
event(wedding).
event(charity_auction).
event(anniversary_party).
event(retirement_banquet).

solve :-
	rose(IdaRose), rose(JeremyRose), rose(HughRose), rose(LeroyRose),
	rose(StellaRose),
	all_different([IdaRose, JeremyRose, HughRose, LeroyRose, StellaRose]),

	event(IdaEvent), event(JeremyEvent), event(HughEvent), event(LeroyEvent),
	event(StellaEvent),
	all_different([IdaEvent, JeremyEvent, HughEvent, LeroyEvent, StellaEvent]),

	item(IdaItem), item(JeremyItem), item(HughItem), item(LeroyItem),
	item(StellaItem),
	all_different([IdaItem, JeremyItem, HughItem, LeroyItem, StellaItem]),

	Quads = [ [ida, IdaRose, IdaEvent, IdaItem],
		  [jeremy, JeremyRose, JeremyEvent, JeremyItem],
		  [hugh, HughRose, HughEvent, HughItem],
		  [leroy, LeroyRose, LeroyEvent, LeroyItem],
		  [stella, StellaRose, StellaEvent, StellaItem] ],
	
	% Jeremy made a purchase for the senior prom.
	member([jeremy, _, senior_prom, _], Quads),
	
	% Stella who did not choose flowers for a wedding, picked the Cottage Beauty
	% variety.
	member([stella, cottage_beauty, _, _], Quads),
	\+ member([stella, _, wedding, _], Quads),

	% Hugh (who selected the Pink Paradise blooms) did not choose flowers for either
	% the charity auction or the wedding.
	member([hugh, pink_paradise, _, _], Quads),
	\+ member([hugh, _, charity_auction, _], Quads),
	\+ member([hugh, _, wedding, _], Quads),

	% The customer who picked roses for an anniversary party also bought streamers.
	member([_, _, anniversary_party, streamers], Quads),

 	% one shopping for a wedding chose the balloons.
	member([_, _, wedding, balloons], Quads),
	
	% The customer who bought the Sweet Dreams variety also bought gourmet chocolates. 
	member([_, sweet_dreams, _, gourmet_chocolates], Quads),

	% Jeremy did not pick the Mountain Bloom variety.
	\+ member([jeremy, mountain_bloom, _, _], Quads),

	% Leroy was shopping for the retirement banquet.
	member([leroy, _, retirement_banquet, _], Quads),

	% The customer in charge of decorating the senior prom also bought the candles.
	member([_, _, senior_prom, candles], Quads),

	tell(ida, IdaRose, IdaEvent, IdaItem),
	tell(jeremy, JeremyRose, JeremyEvent, JeremyItem),
	tell(hugh, HughRose, HughEvent, HughItem),
	tell(leroy, LeroyRose, LeroyEvent, LeroyItem),
	tell(stella, StellaRose, StellaEvent, StellaItem).



% Succeeds if all elements of the argument list are bound and different.
% Fails if any elements are unbound or equal to some other element.
all_different([H | T]) :- member(H, T), !, fail.
all_different([_ | T]) :- all_different(T).
all_different([_]).

tell(W, X, Y, Z) :-
	write(W), write(' brought the '), write(X), write(' rose variety for the '), 
	write(Y), write(' and also ordered the '), write(Z), write('.'), nl.
